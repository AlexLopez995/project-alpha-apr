Project Manager by Alex Lopez

Getting Started
To get a local copy up and running follow these simple steps.

Prerequisites
Before you begin, ensure you have the latest version of Python installed on your system. You can download Python from python.org.

Installation
Fork the Project

Start by forking the repository to your GitHub/Gitlab account. This creates a copy of the repository in your account, allowing you to make changes without affecting the original project.

Clone the Repository

After forking the project, clone it to your local machine. On git, click the "code" button and click "clone with https". Then clone the project by running the following command in your terminal: git clone (paste the clone url here followed by a space and a period)

Navigate to the project directory.

Create a Virtual Environment
To avoid conflicts with other projects or system-wide packages, it's a good idea to use a virtual environment. Create one by running: python -m venv venv

Activate the Virtual Environment
Accomplish this by running:
    -For Mac, run the following code: source .venv/bin/activate
    -For PC, run the following code: .\venv\Scripts\activate

Upgrade Pip
Ensure you have the latest version of pip, the Python Package manager, by running:
    pip install -- upgrade pip

Install Dependencies
Install the project dependencies, including Django, Flake8, Black, and djLint by running:
    pip install django flake8 black djlint

Run the development server
To start the development server and access the application, run:
    python manage.py runserver

Deactivate Virtual Environment
If you at anytime need to deactivate the Virtual Environment, simply fun the following command:
    deactivate

Usage
After running the development server, simply go to the localhost:8000 in your browser, click the option to sign up, make an account, and you will automatically be logged in. You may now browse the site and test its capabilities by creating new projects and adding tasks to your projects.

Running tests
To run the automated tests for this project, execute the following command:
    python manage.py test
